EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8100 3800 0    100  Italic 0
Surface Mount Connector
$Comp
L RPi_Zero_pHat_Template-rescue:LED-Device D1
U 1 1 60B7FD34
P 7150 1600
F 0 "D1" H 7143 1817 50  0000 C CNN
F 1 "LED" H 7143 1726 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Horizontal_O1.27mm_Z2.0mm" H 7150 1600 50  0001 C CNN
F 3 "~" H 7150 1600 50  0001 C CNN
	1    7150 1600
	1    0    0    -1  
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:LED-Device D2
U 1 1 60B859E2
P 7150 2000
F 0 "D2" H 7143 2217 50  0000 C CNN
F 1 "LED" H 7143 2126 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_Horizontal_O3.81mm_Z9.0mm" H 7150 2000 50  0001 C CNN
F 3 "~" H 7150 2000 50  0001 C CNN
	1    7150 2000
	1    0    0    -1  
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R4
U 1 1 60B875ED
P 7600 1600
F 0 "R4" V 7393 1600 50  0000 C CNN
F 1 "330" V 7484 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7530 1600 50  0001 C CNN
F 3 "~" H 7600 1600 50  0001 C CNN
	1    7600 1600
	0    1    1    0   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R5
U 1 1 60B882FD
P 7600 2000
F 0 "R5" V 7393 2000 50  0000 C CNN
F 1 "330" V 7484 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7530 2000 50  0001 C CNN
F 3 "~" H 7600 2000 50  0001 C CNN
	1    7600 2000
	0    1    1    0   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R6
U 1 1 60B885FC
P 7600 2450
F 0 "R6" V 7393 2450 50  0000 C CNN
F 1 "330" V 7484 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7530 2450 50  0001 C CNN
F 3 "~" H 7600 2450 50  0001 C CNN
	1    7600 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 1600 7300 1600
Wire Wire Line
	7900 1700 7900 2000
Wire Wire Line
	7900 2000 7750 2000
Wire Wire Line
	7450 2000 7300 2000
Wire Wire Line
	7950 1800 7950 2450
Wire Wire Line
	7950 2450 7750 2450
Wire Wire Line
	7450 2450 7300 2450
Wire Wire Line
	6550 2900 6550 3900
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R9
U 1 1 60B91223
P 10400 5750
F 0 "R9" H 10470 5796 50  0000 L CNN
F 1 "1000" H 10470 5705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 10330 5750 50  0001 C CNN
F 3 "~" H 10400 5750 50  0001 C CNN
	1    10400 5750
	-1   0    0    1   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R8
U 1 1 60B91A59
P 8000 5800
F 0 "R8" H 8070 5846 50  0000 L CNN
F 1 "4100" H 8070 5755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7930 5800 50  0001 C CNN
F 3 "~" H 8000 5800 50  0001 C CNN
	1    8000 5800
	-1   0    0    1   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R7
U 1 1 60B923E5
P 8000 5200
F 0 "R7" H 8070 5246 50  0000 L CNN
F 1 "7500" H 8070 5155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7930 5200 50  0001 C CNN
F 3 "~" H 8000 5200 50  0001 C CNN
	1    8000 5200
	-1   0    0    1   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR010
U 1 1 60B957D3
P 9300 5100
F 0 "#PWR010" H 9300 4850 50  0001 C CNN
F 1 "GND" V 9305 4972 50  0000 R CNN
F 2 "" H 9300 5100 50  0001 C CNN
F 3 "" H 9300 5100 50  0001 C CNN
	1    9300 5100
	-1   0    0    1   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:D_Zener-Device D4
U 1 1 60B95AF6
P 10400 5100
F 0 "D4" V 10354 5180 50  0000 L CNN
F 1 "4.7" V 10445 5180 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 10400 5100 50  0001 C CNN
F 3 "~" H 10400 5100 50  0001 C CNN
	1    10400 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10400 5900 10400 5950
Wire Wire Line
	10400 5250 10400 5550
Connection ~ 8000 5950
Wire Wire Line
	8000 5950 8600 5950
Wire Wire Line
	8000 5650 8000 5450
Wire Wire Line
	9250 5450 8800 5450
Wire Wire Line
	8800 5450 8800 5650
Wire Wire Line
	9850 5550 10400 5550
Connection ~ 10400 5550
Wire Wire Line
	10400 5550 10400 5600
Wire Wire Line
	9850 5350 9950 5350
Wire Wire Line
	9950 5350 9950 4800
Wire Wire Line
	9950 4800 8550 4800
Wire Wire Line
	8550 4800 8550 5450
Wire Wire Line
	8550 5450 8000 5450
Connection ~ 8000 5450
Wire Wire Line
	8000 5450 8000 5350
Wire Wire Line
	10750 5950 10400 5950
Wire Wire Line
	10750 1500 10750 5950
Connection ~ 10400 5950
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R1
U 1 1 60B9B02D
P 6450 5300
F 0 "R1" V 6243 5300 50  0000 C CNN
F 1 "10K" V 6334 5300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6380 5300 50  0001 C CNN
F 3 "~" H 6450 5300 50  0001 C CNN
	1    6450 5300
	1    0    0    -1  
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R2
U 1 1 60B9B033
P 7000 3900
F 0 "R2" H 7070 3946 50  0000 L CNN
F 1 "39K" H 7070 3855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6930 3900 50  0001 C CNN
F 3 "~" H 7000 3900 50  0001 C CNN
	1    7000 3900
	0    -1   -1   0   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:R-Device R3
U 1 1 60B9B039
P 7500 3900
F 0 "R3" H 7570 3946 50  0000 L CNN
F 1 "1K" H 7570 3855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7430 3900 50  0001 C CNN
F 3 "~" H 7500 3900 50  0001 C CNN
	1    7500 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 5150 6450 4850
Wire Wire Line
	6550 4250 6550 3900
Wire Wire Line
	6550 3900 6850 3900
Wire Wire Line
	6650 4850 6650 5100
Wire Wire Line
	6650 5100 7250 5100
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR07
U 1 1 60B9B050
P 7800 3900
F 0 "#PWR07" H 7800 3650 50  0001 C CNN
F 1 "GND" H 7805 3727 50  0000 C CNN
F 2 "" H 7800 3900 50  0001 C CNN
F 3 "" H 7800 3900 50  0001 C CNN
	1    7800 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 5950 8000 5950
Wire Wire Line
	6450 5450 6450 5950
Connection ~ 7250 3900
Wire Wire Line
	7250 3900 7350 3900
Wire Wire Line
	7150 3900 7250 3900
Wire Wire Line
	7250 5100 7250 3900
Wire Wire Line
	8100 1800 7950 1800
Wire Wire Line
	8100 1700 7900 1700
Wire Wire Line
	8100 1600 7750 1600
Wire Wire Line
	8100 2900 6550 2900
NoConn ~ 8100 3400
NoConn ~ 8100 3300
NoConn ~ 8100 3200
NoConn ~ 8100 3100
NoConn ~ 8100 3000
NoConn ~ 8100 2800
NoConn ~ 8100 2600
NoConn ~ 8100 2500
NoConn ~ 8100 2400
NoConn ~ 8100 2300
NoConn ~ 8100 2200
NoConn ~ 8100 2100
NoConn ~ 8100 2000
NoConn ~ 8100 1900
NoConn ~ 8100 1500
Text Notes 2550 4450 0    100  Italic 0
Thru-Hole Connector
$Comp
L Connector-ML:RPi_GPIO J2
U 1 1 5516AE26
P 2600 2150
F 0 "J2" H 3350 2400 60  0000 C CNN
F 1 "RPi_GPIO" H 3350 2300 60  0000 C CNN
F 2 "RPi_Hat:Pin_Header_Straight_2x20" H 2600 2150 60  0001 C CNN
F 3 "" H 2600 2150 60  0000 C CNN
	1    2600 2150
	1    0    0    -1  
$EndComp
NoConn ~ 2400 2150
NoConn ~ 2400 2250
NoConn ~ 2400 2350
NoConn ~ 2400 2450
NoConn ~ 2400 2550
NoConn ~ 2400 2650
NoConn ~ 2400 2750
NoConn ~ 2400 2850
NoConn ~ 2400 2950
NoConn ~ 2400 3050
NoConn ~ 2400 3150
NoConn ~ 2400 3250
NoConn ~ 2400 3350
NoConn ~ 2400 3450
NoConn ~ 2400 3550
NoConn ~ 2400 3650
NoConn ~ 2400 3750
NoConn ~ 2400 3850
NoConn ~ 2400 3950
NoConn ~ 2400 4050
$Comp
L Connector-ML:RPi_GPIO J1
U 1 1 5515D39E
P 8300 1500
F 0 "J1" H 9050 1750 60  0000 C CNN
F 1 "RPi_GPIO" H 9050 1650 60  0000 C CNN
F 2 "RPi_Hat:Samtec_HLE-120-02-XXX-DV-BE-XX-XX" H 8300 1500 60  0001 C CNN
F 3 "" H 8300 1500 60  0000 C CNN
	1    8300 1500
	1    0    0    -1  
$EndComp
NoConn ~ 10000 1800
NoConn ~ 10000 1900
NoConn ~ 10000 2000
NoConn ~ 10000 2100
NoConn ~ 10000 2200
NoConn ~ 10000 2300
NoConn ~ 10000 2400
NoConn ~ 10000 2500
NoConn ~ 10000 2600
NoConn ~ 10000 2700
NoConn ~ 10000 2800
NoConn ~ 10000 2900
NoConn ~ 10000 3000
NoConn ~ 10000 3100
NoConn ~ 10000 3200
NoConn ~ 10000 3300
NoConn ~ 10000 3400
NoConn ~ 4300 2150
NoConn ~ 4300 2250
NoConn ~ 4300 2350
NoConn ~ 4300 2450
NoConn ~ 4300 2550
NoConn ~ 4300 2650
NoConn ~ 4300 2750
NoConn ~ 4300 2850
NoConn ~ 4300 2950
NoConn ~ 4300 3050
NoConn ~ 4300 3150
NoConn ~ 4300 3250
NoConn ~ 4300 3350
NoConn ~ 4300 3450
NoConn ~ 4300 3550
NoConn ~ 4300 3650
NoConn ~ 4300 3750
NoConn ~ 4300 3850
NoConn ~ 4300 3950
NoConn ~ 4300 4050
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR05
U 1 1 60BE3CFF
P 6950 4800
F 0 "#PWR05" H 6950 4550 50  0001 C CNN
F 1 "GND" V 6955 4672 50  0000 R CNN
F 2 "" H 6950 4800 50  0001 C CNN
F 3 "" H 6950 4800 50  0001 C CNN
	1    6950 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 4650 6250 550 
Wire Wire Line
	6250 550  10250 550 
Wire Wire Line
	10250 550  10250 1500
Wire Wire Line
	10250 1500 10750 1500
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR03
U 1 1 60BEC5A2
P 6650 2450
F 0 "#PWR03" H 6650 2200 50  0001 C CNN
F 1 "GND" V 6655 2322 50  0000 R CNN
F 2 "" H 6650 2450 50  0001 C CNN
F 3 "" H 6650 2450 50  0001 C CNN
	1    6650 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 2450 6800 2450
$Comp
L RPi_Zero_pHat_Template-rescue:LED-Device D3
U 1 1 60B85E0A
P 7150 2450
F 0 "D3" H 7143 2667 50  0000 C CNN
F 1 "LED" H 7143 2576 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_Horizontal_O3.81mm_Z9.0mm" H 7150 2450 50  0001 C CNN
F 3 "~" H 7150 2450 50  0001 C CNN
	1    7150 2450
	1    0    0    -1  
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR02
U 1 1 60BF0515
P 6650 2000
F 0 "#PWR02" H 6650 1750 50  0001 C CNN
F 1 "GND" V 6655 1872 50  0000 R CNN
F 2 "" H 6650 2000 50  0001 C CNN
F 3 "" H 6650 2000 50  0001 C CNN
	1    6650 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 2000 6650 2000
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR01
U 1 1 60BF14AE
P 6650 1600
F 0 "#PWR01" H 6650 1350 50  0001 C CNN
F 1 "GND" V 6655 1472 50  0000 R CNN
F 2 "" H 6650 1600 50  0001 C CNN
F 3 "" H 6650 1600 50  0001 C CNN
	1    6650 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 1600 6650 1600
Wire Wire Line
	10250 1500 10000 1500
Connection ~ 10250 1500
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR011
U 1 1 60BF5B2E
P 10400 4600
F 0 "#PWR011" H 10400 4350 50  0001 C CNN
F 1 "GND" V 10405 4472 50  0000 R CNN
F 2 "" H 10400 4600 50  0001 C CNN
F 3 "" H 10400 4600 50  0001 C CNN
	1    10400 4600
	-1   0    0    1   
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR08
U 1 1 60BF93A9
P 8000 4700
F 0 "#PWR08" H 8000 4450 50  0001 C CNN
F 1 "GND" V 8005 4572 50  0000 R CNN
F 2 "" H 8000 4700 50  0001 C CNN
F 3 "" H 8000 4700 50  0001 C CNN
	1    8000 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	8000 5050 8000 4700
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR04
U 1 1 60BFBC3D
P 6750 6500
F 0 "#PWR04" H 6750 6250 50  0001 C CNN
F 1 "GND" V 6755 6372 50  0000 R CNN
F 2 "" H 6750 6500 50  0001 C CNN
F 3 "" H 6750 6500 50  0001 C CNN
	1    6750 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 6500 6450 6500
Wire Wire Line
	6450 6500 6450 6350
Connection ~ 10000 1500
Wire Wire Line
	10000 1000 10000 1500
Wire Wire Line
	9000 5950 9650 5950
Wire Wire Line
	9650 5750 9650 5950
Connection ~ 9650 5950
Wire Wire Line
	9650 5950 10400 5950
Wire Wire Line
	6950 4800 6950 4650
Wire Wire Line
	6950 4650 6850 4650
$Comp
L power:PWR_FLAG #FLG01
U 1 1 60B99ACA
P 10000 1000
F 0 "#FLG01" H 10000 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 10000 1173 50  0000 C CNN
F 2 "" H 10000 1000 50  0001 C CNN
F 3 "~" H 10000 1000 50  0001 C CNN
	1    10000 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3900 7650 3900
Wire Wire Line
	9650 5150 9650 5100
Wire Wire Line
	9650 5100 9300 5100
$Comp
L power:GNDPWR #PWR09
U 1 1 60BA1996
P 8100 1050
F 0 "#PWR09" H 8100 850 50  0001 C CNN
F 1 "GNDPWR" H 8104 896 50  0000 C CNN
F 2 "" H 8100 1000 50  0001 C CNN
F 3 "" H 8100 1000 50  0001 C CNN
	1    8100 1050
	1    0    0    -1  
$EndComp
$Comp
L RPi_Zero_pHat_Template-rescue:GND-power #PWR06
U 1 1 60BA3DAC
P 7750 1050
F 0 "#PWR06" H 7750 800 50  0001 C CNN
F 1 "GND" V 7755 922 50  0000 R CNN
F 2 "" H 7750 1050 50  0001 C CNN
F 3 "" H 7750 1050 50  0001 C CNN
	1    7750 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	8100 1050 7750 1050
$Comp
L Device:Microphone MK1
U 1 1 60BA7039
P 6450 6150
F 0 "MK1" H 6580 6196 50  0000 L CNN
F 1 "Microphone" H 6580 6105 50  0000 L CNN
F 2 "Sensor_Audio:ST_HLGA-6_3.76x4.72mm_P1.65mm" V 6450 6250 50  0001 C CNN
F 3 "~" V 6450 6250 50  0001 C CNN
	1    6450 6150
	1    0    0    -1  
$EndComp
Connection ~ 6450 5950
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60BA9CE2
P 8800 5850
F 0 "Q1" V 9035 5850 50  0000 C CNN
F 1 "2N2219" V 9126 5850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 9000 5775 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 8800 5850 50  0001 L CNN
	1    8800 5850
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LMH6702MA U2
U 1 1 60BAE26D
P 6550 4550
F 0 "U2" V 6596 4206 50  0000 R CNN
F 1 "LMH6702MA" V 6505 4206 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6450 4350 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmh6702.pdf" H 6700 4700 50  0001 C CNN
	1    6550 4550
	0    -1   -1   0   
$EndComp
$Comp
L Amplifier_Operational:LMH6702MA U1
U 1 1 60BAFAC2
P 9550 5450
F 0 "U1" H 9550 4969 50  0000 C CNN
F 1 "LMH6702MA" H 9550 5060 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9450 5250 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmh6702.pdf" H 9700 5600 50  0001 C CNN
	1    9550 5450
	-1   0    0    1   
$EndComp
NoConn ~ 10000 1600
Wire Wire Line
	10400 4950 10400 4600
NoConn ~ 10000 1700
Wire Wire Line
	8100 2700 6800 2700
Wire Wire Line
	6800 2700 6800 2450
Connection ~ 6800 2450
Wire Wire Line
	6800 2450 6650 2450
Text Notes 7250 6750 0    50   ~ 0
By DVCSINA001, GDSSAM001 and OMRRAY001
Text Notes 7350 7500 0    50   ~ 0
Sex Diagnosis Device 0.11b Schematic\n
Text Notes 8150 7650 0    50   ~ 0
June 2021\n
Connection ~ 6550 3900
$EndSCHEMATC
