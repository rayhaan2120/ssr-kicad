EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Filter_Amplifier_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R1
U 1 1 60B9B02D
P 3800 4800
F 0 "R1" V 3593 4800 50  0000 C CNN
F 1 "10K" V 3684 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 3730 4800 50  0001 C CNN
F 3 "~" H 3800 4800 50  0001 C CNN
	1    3800 4800
	0    1    1    0   
$EndComp
$Comp
L Filter_Amplifier_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R2
U 1 1 60B9B033
P 5200 5350
F 0 "R2" H 5270 5396 50  0000 L CNN
F 1 "39K" H 5270 5305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 5130 5350 50  0001 C CNN
F 3 "~" H 5200 5350 50  0001 C CNN
	1    5200 5350
	1    0    0    -1  
$EndComp
$Comp
L Filter_Amplifier_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R3
U 1 1 60B9B039
P 5200 5850
F 0 "R3" H 5270 5896 50  0000 L CNN
F 1 "1K" H 5270 5805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 5130 5850 50  0001 C CNN
F 3 "~" H 5200 5850 50  0001 C CNN
	1    5200 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4800 4250 4800
Wire Wire Line
	4850 4900 5200 4900
Wire Wire Line
	5200 4900 5200 5200
Wire Wire Line
	4250 5000 4000 5000
Wire Wire Line
	4000 5000 4000 5600
Connection ~ 5200 5600
Wire Wire Line
	5200 5600 5200 5700
Wire Wire Line
	5200 5500 5200 5600
Wire Wire Line
	4000 5600 5200 5600
$Comp
L Filter_Amplifier_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR05
U 1 1 60BE3CFF
P 4300 5300
F 0 "#PWR05" H 4300 5050 50  0001 C CNN
F 1 "GND" V 4305 5172 50  0000 R CNN
F 2 "" H 4300 5300 50  0001 C CNN
F 3 "" H 4300 5300 50  0001 C CNN
	1    4300 5300
	1    0    0    -1  
$EndComp
$Comp
L Filter_Amplifier_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR04
U 1 1 60BFBC3D
P 3200 5450
F 0 "#PWR04" H 3200 5200 50  0001 C CNN
F 1 "GND" V 3205 5322 50  0000 R CNN
F 2 "" H 3200 5450 50  0001 C CNN
F 3 "" H 3200 5450 50  0001 C CNN
	1    3200 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5300 4450 5300
Wire Wire Line
	4450 5300 4450 5200
$Comp
L Device:Microphone MK1
U 1 1 60BA7039
P 3200 5050
F 0 "MK1" H 3330 5096 50  0000 L CNN
F 1 "Microphone" H 3330 5005 50  0000 L CNN
F 2 "Sensor_Audio:ST_HLGA-6_3.76x4.72mm_P1.65mm" V 3200 5150 50  0001 C CNN
F 3 "~" V 3200 5150 50  0001 C CNN
	1    3200 5050
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LMH6702MA U2
U 1 1 60BAE26D
P 4550 4900
F 0 "U2" V 4596 4556 50  0000 R CNN
F 1 "LMH6702MA" V 4505 4556 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4450 4700 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmh6702.pdf" H 4700 5050 50  0001 C CNN
	1    4550 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4850 3200 4800
Wire Wire Line
	3200 4800 3650 4800
Wire Wire Line
	3200 5250 3200 5450
$Comp
L Filter_Amplifier_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR?
U 1 1 60BACB09
P 5200 6300
F 0 "#PWR?" H 5200 6050 50  0001 C CNN
F 1 "GND" V 5205 6172 50  0000 R CNN
F 2 "" H 5200 6300 50  0001 C CNN
F 3 "" H 5200 6300 50  0001 C CNN
	1    5200 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 6000 5200 6300
Wire Wire Line
	4450 4600 4450 4300
Text Notes 4400 4300 0    50   ~ 0
5V\n
$EndSCHEMATC
