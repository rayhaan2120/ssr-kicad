EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED_Submodule-rescue:LED-Device-RPi_Zero_pHat_Template-rescue D1
U 1 1 60B7FD34
P 4650 2950
F 0 "D1" H 4643 3167 50  0000 C CNN
F 1 "LED" H 4643 3076 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Horizontal_O1.27mm_Z2.0mm" H 4650 2950 50  0001 C CNN
F 3 "~" H 4650 2950 50  0001 C CNN
	1    4650 2950
	1    0    0    -1  
$EndComp
$Comp
L LED_Submodule-rescue:LED-Device-RPi_Zero_pHat_Template-rescue D2
U 1 1 60B859E2
P 4650 3350
F 0 "D2" H 4643 3567 50  0000 C CNN
F 1 "LED" H 4643 3476 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_Horizontal_O3.81mm_Z9.0mm" H 4650 3350 50  0001 C CNN
F 3 "~" H 4650 3350 50  0001 C CNN
	1    4650 3350
	1    0    0    -1  
$EndComp
$Comp
L LED_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R4
U 1 1 60B875ED
P 5100 2950
F 0 "R4" V 4893 2950 50  0000 C CNN
F 1 "330" V 4984 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 5030 2950 50  0001 C CNN
F 3 "~" H 5100 2950 50  0001 C CNN
	1    5100 2950
	0    1    1    0   
$EndComp
$Comp
L LED_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R5
U 1 1 60B882FD
P 5100 3350
F 0 "R5" V 4893 3350 50  0000 C CNN
F 1 "330" V 4984 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 5030 3350 50  0001 C CNN
F 3 "~" H 5100 3350 50  0001 C CNN
	1    5100 3350
	0    1    1    0   
$EndComp
$Comp
L LED_Submodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R6
U 1 1 60B885FC
P 5100 3800
F 0 "R6" V 4893 3800 50  0000 C CNN
F 1 "330" V 4984 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 5030 3800 50  0001 C CNN
F 3 "~" H 5100 3800 50  0001 C CNN
	1    5100 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 2950 4800 2950
Wire Wire Line
	4950 3350 4800 3350
Wire Wire Line
	4950 3800 4800 3800
$Comp
L LED_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR03
U 1 1 60BEC5A2
P 4150 3800
F 0 "#PWR03" H 4150 3550 50  0001 C CNN
F 1 "GND" V 4155 3672 50  0000 R CNN
F 2 "" H 4150 3800 50  0001 C CNN
F 3 "" H 4150 3800 50  0001 C CNN
	1    4150 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 3800 4450 3800
$Comp
L LED_Submodule-rescue:LED-Device-RPi_Zero_pHat_Template-rescue D3
U 1 1 60B85E0A
P 4650 3800
F 0 "D3" H 4643 4017 50  0000 C CNN
F 1 "LED" H 4643 3926 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm_Horizontal_O3.81mm_Z9.0mm" H 4650 3800 50  0001 C CNN
F 3 "~" H 4650 3800 50  0001 C CNN
	1    4650 3800
	1    0    0    -1  
$EndComp
$Comp
L LED_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR02
U 1 1 60BF0515
P 4150 3350
F 0 "#PWR02" H 4150 3100 50  0001 C CNN
F 1 "GND" V 4155 3222 50  0000 R CNN
F 2 "" H 4150 3350 50  0001 C CNN
F 3 "" H 4150 3350 50  0001 C CNN
	1    4150 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 3350 4150 3350
$Comp
L LED_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR01
U 1 1 60BF14AE
P 4150 2950
F 0 "#PWR01" H 4150 2700 50  0001 C CNN
F 1 "GND" V 4155 2822 50  0000 R CNN
F 2 "" H 4150 2950 50  0001 C CNN
F 3 "" H 4150 2950 50  0001 C CNN
	1    4150 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 2950 4150 2950
$Comp
L LED_Submodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR04
U 1 1 60BFBC3D
P 1200 7700
F 0 "#PWR04" H 1200 7450 50  0001 C CNN
F 1 "GND" V 1205 7572 50  0000 R CNN
F 2 "" H 1200 7700 50  0001 C CNN
F 3 "" H 1200 7700 50  0001 C CNN
	1    1200 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 7700 900  7700
Wire Wire Line
	900  7700 900  7550
$Comp
L power:PWR_FLAG #FLG01
U 1 1 60B99ACA
P 4450 3800
F 0 "#FLG01" H 4450 3875 50  0001 C CNN
F 1 "PWR_FLAG" H 4450 3973 50  0000 C CNN
F 2 "" H 4450 3800 50  0001 C CNN
F 3 "~" H 4450 3800 50  0001 C CNN
	1    4450 3800
	1    0    0    -1  
$EndComp
Connection ~ 4450 3800
Wire Wire Line
	4150 3800 4450 3800
Wire Wire Line
	5250 2950 5550 2950
Wire Wire Line
	5250 3350 5550 3350
Wire Wire Line
	5250 3800 5550 3800
Text Notes 5600 3800 0    50   ~ 0
GPIO
Text Notes 5600 3350 0    50   ~ 0
GPIO\n
Text Notes 5600 2950 0    50   ~ 0
GPIO\n
$EndSCHEMATC
