EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7400 7500 0    50   ~ 0
Amplifier\n
Text Notes 7500 6900 0    89   ~ 0
By Me, Samuel Goodson
Text Notes 10600 7650 0    89   ~ 0
candice\n
$Comp
L Device:R R1
U 1 1 60B922B1
P 3000 3050
F 0 "R1" V 2793 3050 50  0000 C CNN
F 1 "10K" V 2884 3050 50  0000 C CNN
F 2 "" V 2930 3050 50  0001 C CNN
F 3 "~" H 3000 3050 50  0001 C CNN
	1    3000 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60B94316
P 4400 3600
F 0 "R2" H 4470 3646 50  0000 L CNN
F 1 "39K" H 4470 3555 50  0000 L CNN
F 2 "" V 4330 3600 50  0001 C CNN
F 3 "~" H 4400 3600 50  0001 C CNN
	1    4400 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60B94B7B
P 4400 4100
F 0 "R3" H 4470 4146 50  0000 L CNN
F 1 "1K" H 4470 4055 50  0000 L CNN
F 2 "" V 4330 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 4100
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:OPAMP U?
U 1 1 60B9572C
P 3750 3150
F 0 "U?" H 4094 3196 50  0000 L CNN
F 1 "OPAMP" H 4094 3105 50  0000 L CNN
F 2 "" H 3750 3150 50  0001 C CNN
F 3 "~" H 3750 3150 50  0001 C CNN
F 4 "Y" H 3750 3150 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "X" H 3750 3150 50  0001 L CNN "Spice_Primitive"
	1    3750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3050 3450 3050
Wire Wire Line
	4050 3150 4400 3150
Wire Wire Line
	4400 3150 4400 3450
Wire Wire Line
	4400 3750 4400 3850
Wire Wire Line
	3450 3250 3200 3250
Wire Wire Line
	3200 3850 4400 3850
Wire Wire Line
	3200 3250 3200 3850
Connection ~ 4400 3850
Wire Wire Line
	4400 3850 4400 3950
$Comp
L power:GND #PWR?
U 1 1 60B97EE6
P 4400 4250
F 0 "#PWR?" H 4400 4000 50  0001 C CNN
F 1 "GND" H 4405 4077 50  0000 C CNN
F 2 "" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0001 C CNN
	1    4400 4250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
