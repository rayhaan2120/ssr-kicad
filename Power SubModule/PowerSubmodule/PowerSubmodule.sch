EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PowerSubmodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R9
U 1 1 60B91223
P 4150 3250
F 0 "R9" H 4220 3296 50  0000 L CNN
F 1 "1000" H 4220 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 4080 3250 50  0001 C CNN
F 3 "~" H 4150 3250 50  0001 C CNN
	1    4150 3250
	1    0    0    -1  
$EndComp
$Comp
L PowerSubmodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R8
U 1 1 60B91A59
P 6550 3200
F 0 "R8" H 6620 3246 50  0000 L CNN
F 1 "4100" H 6620 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 6480 3200 50  0001 C CNN
F 3 "~" H 6550 3200 50  0001 C CNN
	1    6550 3200
	1    0    0    -1  
$EndComp
$Comp
L PowerSubmodule-rescue:R-Device-RPi_Zero_pHat_Template-rescue R7
U 1 1 60B923E5
P 6550 3800
F 0 "R7" H 6620 3846 50  0000 L CNN
F 1 "7500" H 6620 3755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 6480 3800 50  0001 C CNN
F 3 "~" H 6550 3800 50  0001 C CNN
	1    6550 3800
	1    0    0    -1  
$EndComp
$Comp
L PowerSubmodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR010
U 1 1 60B957D3
P 5250 3900
F 0 "#PWR010" H 5250 3650 50  0001 C CNN
F 1 "GND" V 5255 3772 50  0000 R CNN
F 2 "" H 5250 3900 50  0001 C CNN
F 3 "" H 5250 3900 50  0001 C CNN
	1    5250 3900
	1    0    0    -1  
$EndComp
$Comp
L PowerSubmodule-rescue:D_Zener-Device-RPi_Zero_pHat_Template-rescue D4
U 1 1 60B95AF6
P 4150 3900
F 0 "D4" V 4104 3980 50  0000 L CNN
F 1 "4.7" V 4195 3980 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 4150 3900 50  0001 C CNN
F 3 "~" H 4150 3900 50  0001 C CNN
	1    4150 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3100 4150 3050
Wire Wire Line
	4150 3750 4150 3450
Wire Wire Line
	6550 3050 5950 3050
Wire Wire Line
	6550 3350 6550 3550
Wire Wire Line
	5300 3550 5750 3550
Wire Wire Line
	5750 3550 5750 3350
Wire Wire Line
	4700 3450 4150 3450
Connection ~ 4150 3450
Wire Wire Line
	4150 3450 4150 3400
Wire Wire Line
	4700 3650 4600 3650
Wire Wire Line
	4600 3650 4600 4200
Wire Wire Line
	4600 4200 6000 4200
Wire Wire Line
	6000 4200 6000 3550
Wire Wire Line
	6000 3550 6550 3550
Connection ~ 6550 3550
Wire Wire Line
	6550 3550 6550 3650
Wire Wire Line
	3800 3050 4150 3050
Connection ~ 4150 3050
$Comp
L PowerSubmodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR011
U 1 1 60BF5B2E
P 4150 4400
F 0 "#PWR011" H 4150 4150 50  0001 C CNN
F 1 "GND" V 4155 4272 50  0000 R CNN
F 2 "" H 4150 4400 50  0001 C CNN
F 3 "" H 4150 4400 50  0001 C CNN
	1    4150 4400
	1    0    0    -1  
$EndComp
$Comp
L PowerSubmodule-rescue:GND-power-RPi_Zero_pHat_Template-rescue #PWR08
U 1 1 60BF93A9
P 6550 4300
F 0 "#PWR08" H 6550 4050 50  0001 C CNN
F 1 "GND" V 6555 4172 50  0000 R CNN
F 2 "" H 6550 4300 50  0001 C CNN
F 3 "" H 6550 4300 50  0001 C CNN
	1    6550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3950 6550 4300
Wire Wire Line
	5550 3050 4900 3050
Wire Wire Line
	4900 3250 4900 3050
Connection ~ 4900 3050
Wire Wire Line
	4900 3050 4150 3050
Wire Wire Line
	4900 3850 4900 3900
Wire Wire Line
	4900 3900 5250 3900
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60BA9CE2
P 5750 3150
F 0 "Q1" V 5985 3150 50  0000 C CNN
F 1 "2N2219" V 6076 3150 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 5950 3075 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 5750 3150 50  0001 L CNN
	1    5750 3150
	0    -1   -1   0   
$EndComp
$Comp
L Amplifier_Operational:LMH6702MA U1
U 1 1 60BAFAC2
P 5000 3550
F 0 "U1" H 5000 3069 50  0000 C CNN
F 1 "LMH6702MA" H 5000 3160 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4900 3350 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmh6702.pdf" H 5150 3700 50  0001 C CNN
	1    5000 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4050 4150 4400
Text Notes 3800 3050 0    50   ~ 0
5V\n
$EndSCHEMATC
