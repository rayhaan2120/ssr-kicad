How to Manufacture this PCB:

1. Download PCB files (for Kicad)
2. Submit relevant files to your preferred PCB manufacturer (provided the support KiCad)
3. Solder any SMD and THT components to their relevant positions on the PCB once you have recieved the PCB. 

BOM: https://gitlab.com/rayhaan2120/ssr-kicad/-/blob/main/RPi_Zero_pHat_Template.csv

If you want to know how to use the device see: https://gitlab.com/rayhaan2120/ssr-kicad/-/blob/main/GettingStarted.md
