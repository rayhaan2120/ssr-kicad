How to use the SD D0.11b PiHat:

1. Insert SD Card into PI zero containing the operating code for the SD D0.11b

2. Put the PiHat on the Pi Zero.

3. Plug the Pi Zero into a usb power bank or phone charger

4. Wait for the Pi to boot. The PiHat will flash its LEDS with a little wave animation,

5. Once the animation is complete the SD D0.11b is ready to speak into and assume the sex of the audio around it.

6. The red LED signifies that the input sound frequency is that of a woman's voice, the green indicates a man's and the blue LED indicates a child. 